package ru.fadeev.tm;

import ru.fadeev.tm.context.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        new Bootstrap().init();
    }

}