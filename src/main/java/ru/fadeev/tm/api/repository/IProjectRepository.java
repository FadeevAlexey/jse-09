package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAll(@NotNull final String userId);

    @NotNull
    List<Project> findAll(@NotNull final String userId);

    @Nullable
    String findIdByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    Collection<Project> findAll(@NotNull final String userId, @NotNull final Comparator<? super Project> selectedSort);

    @NotNull
    Collection<Project> searchByName(@NotNull final String userId, @NotNull final String string);

    @NotNull
    Collection<Project> searchByDescription(@NotNull final String userId, @NotNull final String string);

}
