package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E> {

    @NotNull
    List<E> findAll();

    @NotNull
    E findOne(@NotNull final String id);

    @NotNull
    E remove(@NotNull final String id);

    @Nullable
    E persist(@NotNull final E e);

    @Nullable
    E merge(@NotNull final E e);

    void removeAll();

}