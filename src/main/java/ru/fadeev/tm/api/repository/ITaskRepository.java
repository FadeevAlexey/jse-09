package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskRepository extends IRepository<Task> {

    void removeAll(@NotNull final String userId);

    @NotNull
    Collection<Task> findAll(@NotNull final String userId);

    @NotNull
    Collection<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId);

    @Nullable
    String findIdByName(@NotNull final String name, @NotNull final String userId);

    void removeAllByProjectId(@NotNull final String projectId, @NotNull final String userId);

    void removeAllProjectTask(@NotNull final String userId);

    @NotNull
    Collection<Task> findAll(@NotNull final String userId, @NotNull final Comparator<? super Task> selectedSort);

    @NotNull
    Collection<Task> searchByName(@NotNull final String userId, @NotNull final String string);

    @NotNull
    Collection<Task> searchByDescription(@NotNull final String userId, @NotNull final String string);
}