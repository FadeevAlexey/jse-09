package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    String findIdByName(@Nullable final String name, @Nullable final String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Project> findAll(@NotNull final String userId, @Nullable final Comparator<? super Project> selectedSort);

    @NotNull
    Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string);

    @NotNull
    Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string);

}