package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskService extends IService<Task> {

    void removeAll(@Nullable final String userId);

    @NotNull
    Collection<Task> findAll(@Nullable final String userId);

    @Nullable
    Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId);

    @Nullable
    String findIdByName(@Nullable String name, @Nullable String userId);

    void removeAllByProjectId(@Nullable final String projectId, @Nullable final String userId);

    void removeAllProjectTask(@Nullable final String userId);

    @NotNull
    Collection<Task> findAll(@NotNull final String userId, @Nullable final Comparator<? super Task> selectedSort);

    @NotNull
    Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string);

    @NotNull
    Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string);

}