package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

public interface ITerminalService {

    @Nullable
    String readString();

    @Nullable
    Date readDate() throws ParseException;

    @NotNull
    String dateToString(@Nullable final Date date);

    void println(@Nullable final String string);

    void print(@Nullable final String string);

    void println(@Nullable final Task task);

    void println(@Nullable final Project project);

    void printTaskList(@Nullable final Collection<Task> tasks);

    void printProjectList(@Nullable final Collection<Project> projects);

}
