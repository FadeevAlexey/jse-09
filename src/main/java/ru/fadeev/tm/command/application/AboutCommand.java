package ru.fadeev.tm.command.application;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;

public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean isPermission(@Nullable final User user) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Product information.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("*** ABOUT TASK MANAGER ***");
        terminal.println("Developer:" + Manifests.read("developer") + " " + Manifests.read("email"));
        terminal.println("Product: " + Manifests.read("artifactId"));
        terminal.println("Version: " + Manifests.read("version"));
        terminal.println("Build number: " + Manifests.read("buildNumber"));
        terminal.println("(C) 2020\n");
    }

}