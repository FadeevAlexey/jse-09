package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().removeAll(serviceLocator.getAppStateService().getUserId());
        serviceLocator.getTerminalService().println("[ALL TASKS REMOVE]\n");
    }

}