package ru.fadeev.tm.entity;

import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@ToString
public abstract class AbstractEntity {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

}