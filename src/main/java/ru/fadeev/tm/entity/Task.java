package ru.fadeev.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper=true)
public final class Task extends AbstractEntity {

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

}