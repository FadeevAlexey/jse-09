package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalSortTypeException extends RuntimeException {

    public IllegalSortTypeException(@Nullable final String message) {
        super(message);
    }

}
