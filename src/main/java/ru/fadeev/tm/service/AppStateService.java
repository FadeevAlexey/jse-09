package ru.fadeev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.IllegalUserNameException;

import java.util.*;

public class AppStateService implements IAppStateService {

    @Getter
    @Setter
    @Nullable
    private User user = null;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    @Override
    public Role getRole() {
        return user == null ? null : user.getRole();
    }

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        commands.put(description, abstractCommand);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return commands.get(command);
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public boolean hasPermission(@Nullable final Role... roles) {
        if (roles == null) return false;
        if (user == null) return false;
        @Nullable final List<Role> roleList = Arrays.asList(roles);
        return roleList.contains(user.getRole());
    }

    @NotNull
    @Override
    public String getUserId() {
        if (user == null)
            throw new IllegalUserNameException("Can't find user");
        return user.getId();
    }

}