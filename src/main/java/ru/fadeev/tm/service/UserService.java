package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.api.repository.IUserRepository;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
       super(userRepository);
       this.userRepository = userRepository;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null) return false;
        if (login.isEmpty()) throw new IllegalUserNameException("Login can't be empty");
        return userRepository.isLoginExist(login);
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login){
        if (login == null || login.isEmpty()) return null;
        return userRepository.findUserByLogin(login);
    }

}